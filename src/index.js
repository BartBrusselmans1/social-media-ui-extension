import React, { Fragment, useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { Button, SelectField, Option } from '@contentful/forma-36-react-components';
import { init } from 'contentful-ui-extensions-sdk';
import tokens from '@contentful/forma-36-tokens';
import '@contentful/forma-36-react-components/dist/styles.css';
import './index.css';
import SocialPreview from './components/SocialPreview';
import { SOCIAL_ENUM } from './const/social.const';

const SidebarExtension = ({sdk}) => {
  const [showSocial, setShowSocial] = useState(false);
  const [socialOption, setSocialOption] = useState(SOCIAL_ENUM.FACEBOOK);
  const [seoImage, setSeoImage] = useState(null);

  const getImageId = useCallback((image) => {
    return image.getValue().sys.id;
  }, []);

  useEffect(() => {
    sdk.window.startAutoResizer();

    sdk.space.getAsset(getImageId(sdk.entry.fields.heroImage))
      .then(asset => setSeoImage(asset))
      .catch(e => console.error(e));
  }, [sdk, getImageId]);

  

  const toggleShowSocial = () => {
    setShowSocial(!showSocial)
  }

  const onSelectChange = (e) => {
    e.preventDefault();
    setSocialOption(e.target.value);
  }

  return (
    <Fragment>
      {!showSocial && <Button buttonType="positive" isFullWidth testId="openDialog" onClick={toggleShowSocial}>Show social media preview</Button>}

      {showSocial && (
        <Fragment>
          <SelectField style={{marginBottom: tokens.spacingM}} labelText="Social media" name="socialSite" id="socialSite" onChange={onSelectChange}>
            <Option value="facebook">
              Facebook
            </Option>
            <Option value="twitter">
              Twitter
            </Option>
            <Option value="linkedin">
              LinkedIn
            </Option>
          </SelectField>
          <SocialPreview option={socialOption} data={sdk.entry.fields} image={seoImage} />
          <Button buttonType="negative" isFullWidth testId="openDialog" onClick={toggleShowSocial}>Close</Button>
        </Fragment>
      )}
    </Fragment>
  )
};

SidebarExtension.propTypes = {
  sdk: PropTypes.object.isRequired
}

export const initialize = sdk => {
    ReactDOM.render(<SidebarExtension sdk={sdk} />, document.getElementById('root'));
};

init(initialize);

/**
 * By default, iframe of the extension is fully reloaded on every save of a source file.
 * If you want to use HMR (hot module reload) instead of full reload, uncomment the following lines
 */
// if (module.hot) {
//   module.hot.accept();
// }
