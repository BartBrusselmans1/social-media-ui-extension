import React from 'react';
import PropTypes from 'prop-types';

import './index.css';

const FacebookCard = ({ data, image }) => {

    const { title, description } = data;

    const { fields: { file: { 'en-US': { url: imageSrc } } } } = image
    const { fields: { description: { 'en-US': imageAlt } } } = image

    return (
        <div className="card-seo-facebook">
            <img src={imageSrc} alt={imageAlt} />
            <div className="card-seo-facebook__content">
                <div className="card-seo-facebook__content-url">
                    <span>ditisdesite.be</span>
                </div>
                <div className="card-seo-facebook__content-title">
                    <span>{title.getValue()}</span>
                </div>
                {description && (<div className="card-seo-facebook__content-description">
                    <span>{description.getValue()}</span>
                </div>)}
            </div>
        </div>
    )
};

export default FacebookCard;

FacebookCard.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.object.isRequired,
        description: PropTypes.object.isRequired,
    }),
    image: PropTypes.object.isRequired,
}
