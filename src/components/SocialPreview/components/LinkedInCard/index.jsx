import React from 'react';
import PropTypes from 'prop-types';

import './index.css';

const LinkedInCard = ({ data: { title }, image }) => {
    
    const { fields: { file: { 'en-US': { url: imageSrc } } } } = image
    const { fields: { description: { 'en-US': imageAlt } } } = image

    return (
        <article className="card-seo-linkedin">
            <div className="card-seo-linkedin__image-container">
                <img src={imageSrc} alt={imageAlt} />
            </div>
            <div className="card-seo-linkedin__content">
                <div className="flex-grow-1 display-flex flex-column">
                    <h2 className="card-seo-linkedin__content-title">
                        <span>{title.getValue()}</span>
                    </h2>
                    <h3 className="card-seo-linkedin__content-url">
                        ditisdesite.be - 2 min read
                    </h3>
                </div>
            </div>
        </article>
    )
};

export default LinkedInCard;

LinkedInCard.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.object.isRequired,
    }),
    image: PropTypes.object.isRequired,
}