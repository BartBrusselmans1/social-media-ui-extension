import React from 'react';
import PropTypes from 'prop-types';

import './index.css';

const TwitterCard = ({ data, image }) => {

    const { title, description } = data;

    const { fields: { file: { 'en-US': { url: imageSrc } } } } = image
    const { fields: { description: { 'en-US': imageAlt } } } = image

    return (
        <div className="card-seo-twitter mb-16">
            <div className="card-seo-twitter card-seo-twitter__content-container">
                <div aria-hidden={true} className="card-seo-twitter card-seo-twitter__image-container">
                    <img src={imageSrc} alt={imageAlt} />
                </div>
                <div className="card-seo-twitter">
                    <span className="card-seo-twitter card-seo-twitter__content">
                        <div className="card-seo-twitter card-seo-twitter__content-container--inner">
                            <div className="card-seo-twitter card-seo-twitter__content-title">
                                <span>{title.getValue()}</span>
                            </div>
                            {description && (<div className="card-seo-twitter card-seo-twitter__content-description__container">
                                <div className="card-seo-twitter__content-description">
                                    <span>{description.getValue()}</span>
                                </div>
                            </div>)}
                            <div className="card-seo-twitter card-seo-twitter__content-url__container">
                                <span className="card-seo-twitter__content-url">
                                    <span>ditisdesite.be</span>
                                </span>
                            </div>
                        </div>
                    </span>
                </div>
            </div>
        </div>
    )
};

export default TwitterCard;

TwitterCard.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.object.isRequired,
        description: PropTypes.object.isRequired,
    }),
    image: PropTypes.object.isRequired,
}
