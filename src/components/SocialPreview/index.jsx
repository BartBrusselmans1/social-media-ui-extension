import React from 'react';
import PropTypes from 'prop-types';
import FacebookCard from './components/FacebookCard';
import { SOCIAL_ENUM } from '../../const/social.const';
import TwitterCard from './components/TwitterCard';
import LinkedInCard from './components/LinkedInCard';

const SocialPreview = ({option, data, image}) => {
    switch (option) {
        case SOCIAL_ENUM.FACEBOOK:
            return <FacebookCard data={data} image={image} />

        case SOCIAL_ENUM.TWITTER:
            return <TwitterCard data={data} image={image} />

        case SOCIAL_ENUM.LINKEDIN:
            return <LinkedInCard data={data} image={image} />

        default:
            break;
    }
};

export default SocialPreview;

SocialPreview.propTypes = {
    option: PropTypes.oneOf(['facebook', 'twitter', 'linkedin']).isRequired,
    data: PropTypes.object.isRequired,
    image: PropTypes.object.isRequired,
}