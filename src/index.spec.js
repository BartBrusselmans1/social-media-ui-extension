import React from 'react';
import { render, fireEvent, cleanup, configure } from '@testing-library/react';
import { SidebarExtension } from './index';

configure({
  testIdAttribute: 'data-test-id'
});

describe('SidebarExtension', () => {
  afterEach(cleanup);

  it('render button', () => {
    const sdk = {
      window: {
        startAutoResizer: jest.fn()
      },
      dialogs: {
        openExtension: jest.fn()
      }
    };
    const { getByTestId } = render(<SidebarExtension sdk={sdk} />);

    expect(sdk.window.startAutoResizer).toHaveBeenCalled();

    fireEvent.click(getByTestId('open-dialog'));

    expect(sdk.dialogs.openExtension).toHaveBeenCalledWith({
      title: 'The same extension rendered in modal window',
      width: 800
    });
  });
});
